---
source: sections/_guide_blocks/03.02-conditions.md
bookmark: conditions
name: Condition blocks
title: Implementing a condition block
sub: true
---

Condition blocks are used to create condition building blocks for the builder. The minimal implementation should look like this:

```typescript
import {
  ConditionBlock, _, tripetto
} from "{{ site.npm_packages.builder }}";
import * as ICON from "./condition.svg";

@tripetto({
    type: "condition",
    identifier: "example-condition",
    context: "*",
    icon: ICON,
    label: () => _("Example condition block")
})
export class Example extends ConditionBlock {}

```

Let's have a closer look at this. We define our condition block by implementing the `ConditionBlock` abstract class, prefixed with the `@tripetto` decorator to supply (meta) information about the block.

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
