---
source: sections/_guide_blocks/05-translations.md
bookmark: translations
name: Translations
title: Translating your block
---

If you have followed the localization guidelines from the previous chapter, your block is ready to be translated. Now you can follow these steps to get you going (we assume you've started your block implementation using the [boilerplate](#boilerplate)):

1. First, update the `./translations/sources` file and add the source files that need to be translated (each file on a separate line);
2. Next, generate the POT file by executing the following command: `npm run pot` (this command uses the `xgettext` tool, make sure it is installed!);
3. Verify the file `./translations/template.pot` is generated. It contains the strings to be translated. Now use this template to create translations. You can use a tool for this (like [Poedit](https://poedit.net){:target="_blank"}) or send the file to a [translation service](https://lingohub.com){:target="_blank"}. Each translation should be stored in a `PO`-file. The name of the `PO`-file should be the locale of that language, for example `nl.PO`;
4. Add the `PO`-files to the `./translations` folder of your block;
5. Convert the `PO`-files to `JSON`-files by executing the command: `npm run make:po2json`;
6. Make sure to include the generated `JSON` files in the `./translations` folder of your distributable block package.
