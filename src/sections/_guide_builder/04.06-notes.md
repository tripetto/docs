---
source: sections/_guide_builder/04.06-notes.md
bookmark: library-notes
name: Notes
title: Notes
sub: true
code: |
  ``` javascript
  import { Builder } from "tripetto";

  const builder = new Builder();

  // Upon window resize notify the builder
  window.addEventListener("resize", () => builder.resize());
  window.addEventListener("orientationchange", () => builder.resize());
  ```
  {: title="Example" }
---

- If you host the builder in a custom HTML element (using the `element` option), you should notify the builder when the dimensions of that element are changed (for example when the browser window is resized or when the display orientation is changed). To do so, you should invoke the `resize` method on the `Builder`-instance. This way the builder can adapt to the new viewport. If you don't supply a custom element, the builder is placed under the `body` element and it will detect a dimension change automatically.
