---
source: sections/_guide_builder/04.02-definitions.md
bookmark: library-definitions
name: Loading + saving
title: Loading and saving form definitions
sub: true
---

### Loading a form definition
To open an builder with a form definition, all you need to do is supply that form definition to the `open` method:
```javascript
import { Builder } from "{{ site.npm_packages.builder }}";

const definition = // Supply your form definition here

// Open the builder for the definition
Builder.open(definition);
```

### Saving or retrieving a form definition
To retrieve the definition back from the builder, you have two options:

- #### Retrieve when the user clicks the save button
Attach a listener to the `onSave` event (this event is invoked when the user clicks the save button in the UI):

```javascript
import { Builder, IDefinition } from "{{ site.npm_packages.builder }}";

const builder = new Builder.open();

// Open the builder and listen for the `OnSave` event
builder.onSave = (definition: IDefinition) => console.dir(definition);
```

- #### Retrieve on each change
You can also monitor for changes in the definition (if you want you can hide the save button from the UI).

```javascript
import { Builder } from "{{ site.npm_packages.builder }}";

const builder = new Builder.open();

// Open the builder and monitor for changes using the `OnChange` event
builder.onChange = (definition: IDefinition) => console.dir(definition);
```
