---
source: sections/_guide_builder/03-cli.md
bookmark: cli
title: Command line tool
---

### For quick stand-alone form creation and testing

The CLI version of the builder is an easy to use command line tool that allows you to create and edit form definition files stored locally on your disk. This is ideal for developers implementing the [runner](../runner) in a website or application, or when building and testing custom [blocks](../blocks) for Tripetto.
