---
source: sections/_guide_builder/06-issues.md
bookmark: issues
title: Issues
---

Run into issues using the builder? Report them [here](https://gitlab.com/{{ site.accounts.gitlab }}/builder/issues){:target="_blank"}.

Take a look at our [support page](../../support/) for more support options.
