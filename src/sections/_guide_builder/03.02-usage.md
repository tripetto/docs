---
source: sections/_guide_builder/03.02-usage.md
bookmark: cli-usage
name: Usage
title: Using the CLI version
sub: true
---

You start the builder from the terminal/command prompt. It runs in any modern browser. You need to specify the filename of the form definition you want to create or edit. It can be any filename you like. The *[form definition](#definitions)* contains the complete structure of your form.

Let's say you want to create a new form definition with the filename `demo.json`. Run the following command to do so:
```bash
$ tripetto demo.json
```

This will create the file in the current working directory if it does not already exist there, or load the file if it is indeed available. A server instance at `localhost` port `3333` is then started. And probably your default browser will automatically open `http://localhost:3333`. If not, open the browser of your choice and navigate to this URL.

There are some additional command line options you can use. Please, find these [here](#cli-options).
