---
source: sections/_quickstart/03-use-cases.md
bookmark: use-cases
title: Use cases
---

#### Stand-alone form creation and deployment
Install and run the builder locally to instantly create and edit powerful forms. Handily deploy these in any website or application with our supplementary form deployment library (i.e. the [runner](guide/runner/)). We also offer ready-to-go implementations for [React](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react){:target="_blank"}, [Angular](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular){:target="_blank"}, [Material-UI](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react-material-ui){:target="_blank"}, [Angular Material](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular-material){:target="_blank"} and [more](https://gitlab.com/{{ site.accounts.gitlab }}/examples){:target="_blank"}.

#### Integrated into your project
Seamlessly integrate the builder into your software and unlock its powerful UI and capabilities as part of your own application. Decide for yourself how and where you want to run the created forms and store response. And while you're at it, why not tweak the visuals to match your brand?

#### Survey creation
Tripetto is also a fierce survey tool with advanced logic and conditional flow functionalities, making it your designated (stand-alone or integrated) solution for building highly responsive and conversational surveys.

#### Custom extensions
You can extend Tripetto with your own building blocks. We supply [documentation](guide/blocks/), [code samples](examples/) and a [boilerplate](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/boilerplate){:target="_blank"} to develop blocks from scratch.
