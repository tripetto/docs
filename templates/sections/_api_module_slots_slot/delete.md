---
source: sections/_api_module_slots_slot/delete.md
title: delete
bookmark: delete
description: Deletes a slot.
endpoint: method
signature: true
---

#### Signature
##### **delete**(): *this*
{: .signature }

#### Returns
Returns a reference to the instance.
