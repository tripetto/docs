---
source: sections/_api_module_slots_slot/pipable.md
title: pipable
bookmark: pipable
description: Contains if this slot is pipable.
endpoint: property
signature: true
---

#### Signature
##### **pipable**?: *TPipable*
{: .signature }
