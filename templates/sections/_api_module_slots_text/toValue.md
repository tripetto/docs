---
source: sections/_api_module_slots_text/toValue.md
title: toValue
bookmark: to-value
description: Converts the supplied data to a valid text value.
endpoint: method
signature: true
---

#### Signature
##### **toValue**(data: *TSerializeTypes*): *string*
{: .signature }

`data` TSerializeTypes
: Specifies the data.

#### Returns
Returns the text value.
