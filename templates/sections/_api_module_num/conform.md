---
source: sections/_api_module_num/conform.md
title: conform
bookmark: conform
description: Conforms the supplied number to the specified precision.
endpoint: function
signature: true
code: |
  ``` typescript
  conform(1.1235, 3); // Returns `1.124`
  conform(1.1235, 2); // Returns `1.12`
  ```
---

#### Signature
##### Num.**conform**(value: *number*, precision: *number*): *number*
{: .signature }

`value` number
: Input number.

`precision` number
: Specifies the number of decimals.

#### Returns
Returns the conformed number.
