---
source: sections/_api_builder_templates/explanation.md
title: explanation
bookmark: explanation
description: Adds the explanation feature.
endpoint: method
signature: true
---

#### Signature
##### **explanation**(block: *NodeBlock | Node*, controller: *BuilderController*): *Feature*
{: .signature }

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
