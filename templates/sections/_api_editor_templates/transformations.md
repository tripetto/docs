---
source: sections/_api_builder_templates/transformations.md
title: transformations
bookmark: transformations
description: Adds the transformations feature.
endpoint: method
signature: true
---

#### Signature
##### **transformations**(text: *Text | undefined*, controller: *BuilderController*): *Feature | undefined*
{: .signature }

`text` Text | undefined
: Reference to a text.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
