---
source: sections/_api_module_slots_boolean/toValue.md
title: toValue
bookmark: to-value
description: Converts the supplied data to a valid boolean value.
endpoint: method
signature: true
---

#### Signature
##### **toValue**(data: *TSerializeTypes*): *boolean*
{: .signature }

`data` TSerializeTypes
: Specifies the data.

#### Returns
Returns the boolean value.
