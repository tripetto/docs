---
source: sections/_api_builder_builder/edit.md
title: edit
bookmark: edit
description: Edits the definition properties.
endpoint: method
signature: true
---

#### Signature
##### **edit**(): *this*
