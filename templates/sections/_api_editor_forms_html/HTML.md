---
source: sections/_api_builder_forms_html/HTML.md
title: HTML
bookmark: html
description: Retrieves or specifies the HTML code.
endpoint: property
signature: true
---

#### Signature
##### **HTML**: *string*
{: .signature }
