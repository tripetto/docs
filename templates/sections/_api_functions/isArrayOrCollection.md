---
source: sections/_api_functions/isArrayOrCollection.md
title: isArrayOrCollection
bookmark: is-array-or-collection
description: Validates if the supplied variable is an array or a collection.
endpoint: function
signature: true
---

#### Signature
##### **isArrayOrCollection**(array: *any*): *boolean*
{: .signature }

`array` any
: Variable to validate.

#### Returns
Returns `true` if the variable is an array or collection.
