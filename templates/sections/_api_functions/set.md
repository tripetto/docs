---
source: sections/_api_functions/set.md
title: set
bookmark: set
description: Sets the value of the specified variable in the object using its index type.
endpoint: function
signature: true
---

#### Signature
##### **set**\<T, K\>(object: *T*, name: *K*, value: *T[K]*): *T[K]*
{: .signature }

`object` T
: Reference to the object.

`name` K
: Name of the variable.

`value` T[K]
: Value for the variable.

#### Returns
Returns a reference to the value on the object.
