---
source: sections/_api_functions/lastArrayItem.md
title: lastArrayItem
bookmark: last-array-item
description: Returns the last item in the array, collection or enumerable object.
endpoint: function
signature: true
code: |
  ``` typescript
  lastArrayItem<number>([1, 2, 3]); // Returns `3`
  ```
---

#### Signature
##### **lastArrayItem**\<T\>(array: *TList\<T\> | undefined*, default?: *T*): *T | undefined*
{: .signature }

`array` TList\<T\> | undefined
: Specifies the array, collection or enumerable object.

`default` Optional T
: Optional parameter which specifies the default value if there is no last item.

#### Returns
Returns the last item.
