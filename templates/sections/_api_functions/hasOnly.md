---
source: sections/_api_functions/hasOnly.md
title: hasOnly
bookmark: has-only
description: Verifies all items of an array, collection or enumerable object against the truth function.
endpoint: function
signature: true
code: |
  ``` typescript
  hasOnly<number>([1, 2, 3], (nItem: number) => nItem > 0); // Returns `true`
  hasOnly<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `false`
  ```
---

#### Signature
##### **hasOnly**\<T\>(list: *TList\<T\> | undefined*, truth: *function*): *boolean*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value with the individual verification result.

#### Returns
Returns `true` when one or more items pass the verification test.
