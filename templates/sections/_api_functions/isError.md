---
source: sections/_api_functions/isError.md
title: isError
bookmark: is-error
description: Validates if the supplied variable is an error object.
endpoint: function
signature: true
code: |
  ``` typescript
  isError(new Error("Test message")); // Returns `true`
  ```
---

#### Signature
##### **isError**(error: *any*): *boolean*
{: .signature }

`error` any
: Variable to validate.

#### Returns
Returns `true` if the variable is an error object.
