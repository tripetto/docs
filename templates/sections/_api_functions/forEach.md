---
source: sections/_api_functions/forEach.md
title: forEach
bookmark: for-each
description: Selects items from an array, collection or enumerable object using a truth test and optionally invokes a function for each item.
endpoint: function
signature: true
code: |
  ``` typescript
  forEach<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `[2, 3]`
  forEach<number>([1, 2, 3], (nItem: number) => nItem > 1, {
    do: (nItem: number) => nItem + 1,
    replace: true
  }); // Returns `[3, 4]`
  ```
---

#### Signature
##### **forEach**\<T\>(list: *TList\<T\> | undefined*, truth: *function*, options?: *object*): *T[] | T | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the optional selector function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value whether to include the item in the selection. If omitted all items are selected.

`options` Optional object
: Contains the options.

#### Returns
Returns an array with the selected items or just the selected item if the single mode is enabled.
