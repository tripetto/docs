---
source: sections/_api_functions/isBoolean.md
title: isBoolean
bookmark: is-boolean
description: Validates if the supplied variable is a boolean.
endpoint: function
signature: true
code: |
  ``` typescript
  isBoolean(false); // Returns `true`
  isBoolean(1); // Returns `false`
  ```
---

#### Signature
##### **isBoolean**(bool: *any*): *boolean*
{: .signature }

`bool` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a boolean.
