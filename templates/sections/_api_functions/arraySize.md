---
source: sections/_api_functions/arraySize.md
title: arraySize
bookmark: array-size
description: Returns the array size for the supplied array or the number of items in the object.
endpoint: function
signature: true
code: |
  ``` typescript
  arraySize([]); // Returns `0`
  arraySize([1, 2, 3]); // Returns `3`
  ```
---

#### Signature
##### **arraySize**(array: *any[]*): *number*
{: .signature }

`array` any[]
: Specifies the array.

#### Returns
Returns the size of the array/object as a number.
