---
source: sections/_api_functions/get.md
title: get
bookmark: get
description: Retrieves the value of the specified variable in the object using its index type.
endpoint: function
signature: true
---

#### Signature
##### **get**\<T, K\>(object: *T*, name: *K*): *T[K]*
{: .signature }

`object` T
: Reference to the object.

`name` K
: Name of the variable.

#### Returns
Returns the value of the variable.
