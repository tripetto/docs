---
source: sections/_api_builder_forms_button/blur.md
title: blur
bookmark: blur
description: Blurs the focus of the control.
endpoint: method
signature: true
---

#### Signature
##### **blur**(): *void*
{: .signature }

#### Returns
Nothing
