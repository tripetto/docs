---
source: sections/_api_builder_templates_collection/constructor.md
title: constructor
bookmark: constructor
description: Creates a new collection card.
endpoint: constructor
signature: true
---

#### Signature
##### new **Collection**(configuration: *ICollectionConfiguration\<T, Reference\>*): *Collection*
{: .signature }

`configuration` ICollectionConfiguration\<T, Reference\>
: Specifies the configuration for the collection.

#### Returns
`Collection`
