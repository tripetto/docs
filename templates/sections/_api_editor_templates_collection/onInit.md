---
source: sections/_api_builder_templates_collection/onInit.md
title: onInit
bookmark: on-init
description: Initializes the card.
endpoint: method
signature: true
---

#### Signature
##### **onInit**(): *boolean*
{: .signature }

#### Returns
Returns the intialization state.
