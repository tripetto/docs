---
source: sections/_api_builder_forms_numeric/max.md
title: max
bookmark: max
description: Sets a maximum value.
endpoint: method
signature: true
---

#### Signature
##### **max**(max: *number*): *this*
{: .signature }

`max` number
: Specifies the maximum value.

#### Returns
Returns a reference to the control to allow chaining.
