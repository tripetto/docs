---
source: sections/_api_builder_forms_numeric/prefix.md
title: prefix
bookmark: prefix
description: Specifies the prefix.
endpoint: method
signature: true
---

#### Signature
##### **prefix**(prefix: *string*): *this*
{: .signature }

`prefix` string
: Specifies the prefix string.

#### Returns
Returns a reference to the control to allow chaining.
