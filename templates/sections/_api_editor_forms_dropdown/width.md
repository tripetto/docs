---
source: sections/_api_builder_forms_dropdown/width.md
title: width
bookmark: width
description: Sets the width of the control.
endpoint: method
signature: true
---

#### Signature
##### **width**(width: *number | "auto" | "full"*): *this*
{: .signature }

`width` number | "auto" | "full"
: Specifies the control width in pixels or sets the width to `auto` or `full`.

#### Returns
Returns a reference to the control to allow chaining.
