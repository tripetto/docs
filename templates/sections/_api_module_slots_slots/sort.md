---
source: sections/_api_module_slots_slots/sort.md
title: sort
bookmark: sort
description: Sorts the slots.
endpoint: method
signature: true
---

#### Signature
##### **sort**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the order of the slots is changed.
