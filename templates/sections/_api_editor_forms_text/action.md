---
source: sections/_api_builder_forms_text/action.md
title: action
bookmark: action
description: Attaches an key action to the control.
endpoint: method
signature: true
---

#### Signature
##### **action**(key: *string*, action: *function*): *this*
{: .signature }

`key` string
: Specifies the key to bind the action to.

`action` function
: Specifies the action function to execute.

#### Returns
Returns a reference to the control to allow chaining.
