---
source: sections/_api_module_str/padRight.md
title: padRight
bookmark: pad-right
description: Converts source variable to a string and pads the string on the right with the supplied number of characters.
endpoint: function
signature: true
code: |
  ``` typescript
  padRight("ABC", "A", 5); // Returns `ABCAA`
  ```
---

#### Signature
##### Str.**padRight**(value: *string | number*, fill: *string*, length: *number*, crop?: *boolean*): *string*
{: .signature }

`value` string | number
: Specifies the value to pad.

`fill` string
: Contains the string which will be used as fill string.

`length` number
: Specifies the desired string length.

`crop` Optional boolean
: Optional boolean value which enables string cropping if the source string length is larger than the desired string length.

#### Returns
Returns the padded string.
