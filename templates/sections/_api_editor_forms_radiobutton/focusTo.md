---
source: sections/_api_builder_forms_radiobutton/focusTo.md
title: focusTo
bookmark: focus-to
description: Sets the focus to the previous or next option.
endpoint: method
signature: true
---

#### Signature
##### **focusTo**(to: *"previous" | "next"*): *boolean*
{: .signature }

`to` "previous" | "next"
: Specifies if the focus is set to the previous or next option.

#### Returns
Returns `true` if the focus is captured by an option.
