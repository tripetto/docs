---
source: sections/_api_builder_forms_radiobutton/optionDisabled.md
title: optionDisabled
bookmark: option-disabled
description: Sets or retrieves the disabled state of the specified button option (or the current option if no option value is specified).
endpoint: method
signature: true
---

#### Signature
##### **optionDisabled**(value?: *T*, disabled?: *boolean*): *boolean*
{: .signature }

`value` Optional T
: Specifies the value of the option to get or set.

`disabled` Optional boolean
: Specifies the disabled state.

#### Returns
Returns the disabled state.
