---
source: sections/_api_builder_forms_datetime/dayOfMonth.md
title: dayOfMonth
bookmark: day_of_month
description: Retrieves or specifies the day of the month.
endpoint: property
signature: true
---

#### Signature
##### **dayOfMonth**: *TDays*
{: .signature }
