---
source: sections/_api_builder_forms_datetime/showSecondSelector.md
title: showSecondSelector
bookmark: show_second_selector
description: Shows the seconds selector.
endpoint: method
signature: true
---

#### Signature
##### **showSecondSelector**(): *void*
{: .signature }

#### Returns
Nothing
