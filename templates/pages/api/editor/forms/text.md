---
base: ../../../../
permalink: /api/builder/forms/text/
title: Text / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_text | sort: "path" %}

<section>
    <section>
        <h2>
            Text
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form text control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
