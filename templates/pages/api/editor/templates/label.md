---
base: ../../../../
permalink: /api/builder/templates/label
title: Label / Templates / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_templates_label | sort: "path" %}

<section>
    <section>
        <h2>
            Templates - Label
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Lorem ipsum.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
