---
base: ../../../
permalink: /api/common/l10n/
title: Localization helper functions / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_l10n | sort: "path" %}

<section>
    <section>
        <h2>
            Localization helper functions
            <span class="endpoint module"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Localization toolkit based on the GNU gettext approach.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
