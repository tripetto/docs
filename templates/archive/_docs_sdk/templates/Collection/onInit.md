---
---

#### Signature
##### **onInit**(): *boolean*
{: .signature }

#### Description
Initializes the card.

#### Returns
Returns the intialization state.
