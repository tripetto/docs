
---
---

#### Signature
##### **transformations**(text: *Text | undefined*, controller: *BuilderController*): *Feature | undefined*
{: .signature }

#### Description
Adds the transformation feature.

`text` Text | undefined
: Reference to a text.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
